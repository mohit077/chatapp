const jwt  = require('jsonwebtoken');
const config = process.env

const verifytoken = (token) => {
    console.log("Token:-", token);
    if(!token) return {error: "Unauthorised Access"}
    try {
        const user = jwt.verify(token, process.env.TOKEN_KEY);
        return user;
    }catch (ex) {
        console.log(ex);
        return {error: "Unauthorised Access!!!"}
    }
}

module.exports = verifytoken;