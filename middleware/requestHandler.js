const {signUp} = require('../controller/user');
const {login} = require('../controller/user');
const {joinRoom} = require('../controller/joinRoom');
const {sendMessage} = require('../controller/sendMessage');

const requestHandler = (body, socket, io) => {
    const {en, data, token} = body;
    switch (en) {
        case 'signUp':
            console.log('signUp',body);
            signUp(data, socket);
            break;
        case 'login':
            console.log('Login', body);
            login(data, socket);
            break;
        case 'joinRoom':
            console.log('join Room', body);
            joinRoom(data, socket);
            break;
        case 'sendMessage':
            console.log('sendMessage', body);
            sendMessage(data, socket);
            break;
    }
}


module.exports = requestHandler;