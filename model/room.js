const { Schema, model, Types } = require('mongoose');

const RoomSchema = new Schema({
    room: {
        type: String,
    },
    users: [{
        type: Types.ObjectId,
        ref: 'User',
    }],
    message: {
        type: [],
        default: []
    },
    createdBy: {
        type: Types.ObjectId,
        ref: 'User',
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    }
});


module.exports = model('Rooms', RoomSchema);