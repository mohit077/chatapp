const { Schema, model } = require('mongoose');


const UserSchema = new Schema({
    first_name: {
        type: String,
        default: null,
    },
    last_name: {
        type: String, 
        default: null,
    },
    email: {
        type: String,
        unique: true, 
    },
    password: {
        type: String,
    },
    socketID: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    },
    status: {
        type: String,
        default: 'offline'
    } 
});


module.exports = model('User', UserSchema);