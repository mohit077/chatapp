const responseHandler = require('../middleware/responseHandler');
const User = require('../model/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const verifytoken = require('../middleware/auth');

const signUp = async (data, socket) => {
    const encryptedPassword =  await bcrypt.hash(data.password, 10);
    console.log(encryptedPassword);
    console.log("test");
    console.log('SocketID:', socket.id);
    const userdata = new User({
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        password: encryptedPassword,
        socketID: socket.id,
        createdAt: data.createdAt,
        updatedAt: data.updatedAt
    });

    const user = await userdata.save();
    console.log('DATA ADDED');
    responseHandler(socket, 'signUp', {success: true, message: 'User Add...', data: { user }});
}

const login = async (data, socket) => {
    try {
        let user = await User.findOne({email: data.email});
        console.log('User', user);

        if(user) {
            user = await User.findOneAndUpdate({email: data.email}, {
            $set: {
                    socketID: socket.id,
                    status: 'online'
                }
            }, {new : true})

            const validatePassword = await bcrypt.compare(data.password, user.password)

            if(validatePassword) {
                const token = jwt.sign(
                    {user_id: user.id, email: data.email, socketID: socket.id}, process.env.TOKEN_KEY
                );
                const userID = verifytoken(token);
                console.log('User:-', userID);
                const sender = userID.user_id;

                // socket data save
                socket.userID = sender
                console.log('SenderID:-', sender);

                responseHandler(socket, { success: true, data: {user, token }, message: 'Login SuccessFully!'});
            }
            else {
                responseHandler(socket, {success: false, message: "Invalid Creadentails"});
            }
        }
    } catch (error) {
        console.log(error);
    }
}

module.exports= {
    signUp,
    login
}