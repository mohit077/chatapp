const Room = require('../model/room');
const verifytoken = require('../middleware/auth');
const responseHandler = require('../middleware/responseHandler');


const sendMessage = async (data, socket) => {

    const user = verifytoken(data.token);
    const sender = user.user_id;
    console.log("SenderID:-", sender);

    let roomFind;
    roomFind = await Room.findOne({ _id: data.roomID, users: sender }).populate({ path: 'users', select: 'firstname status' });
    console.log('Room Find:', roomFind);

    if(roomFind){
        const roomData = await Room.findByIdAndUpdate({ _id: roomFind._id }, {
        $push: {
            message: [{
                sender: sender,
                message: data.message
            }]
        }
    }, { new: true, upsert: true }).populate({ path: 'users', select: 'firstname' });

        responseHandler(socket, 'sendMessage', {success: true, data: {roomData}, message: 'Message Sent..!!'});

        const room = data.roomID.toString();
        console.log('Room', room);
        socket.emit('res', { data:data.message });
        socket.to(room).emit('res', { data: data.message });

        const read = readMsg(roomFind);
        responseHandler(socket, 'readMsg', { success: true, message: 'Read By...', data: {read} });

    } else {
        responseHandler(socket, 'sendMessage', { success: false, message: 'Room Not Found...' });
    }

}

function readMsg(roomFind) {
    console.log(JSON.stringify(roomFind));

    let data1 = roomFind.users;
    console.log('DATA1:-', data1);

    let readBy = []
    data1.forEach(element => {
        const a = element.status;
        if(a == 'online') {
            readBy.push(element.firstname);
        }
    });
    console.log('User:',readBy);
    return readBy;
}


module.exports = {
    sendMessage
}