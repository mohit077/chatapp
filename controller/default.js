const User = require('../model/user');

exports.disconnectHandler = async (socket) => {
    console.log('DisconnectTD', socket.userID);
    const user = await User.findById({_id: socket.userID});
    console.log('User', user);
    if(user){
        user = await User.findByIdAndUpdate({_id: socket.userID}, {
            $set: {
                status: 'offline'
            }
        }, { new: true, upsert: true});
    }
}