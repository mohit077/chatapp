const Room = require('../model/room');
const responseHandler = require('../middleware/responseHandler');
const verifytoken = require('../middleware/auth');

const joinRoom = async (data,socket) => {

    const user = verifytoken(data.token);
    console.log('User:', user);

    const sender = user.user_id;
    console.log('SenderID:-', sender);

    console.log("RoomID", data.roomID);
    
    let roomFind;
    if(data.roomID){
        console.log(data.roomID);
        roomFind = await Room.findById({ _id: data.roomID });
        console.log('Find Room:', roomFind);
    }
    

    if (!roomFind) {
        const roomData = new Room({
            room:  data.room,
            users: sender,
            createdBy: sender
        });
        roomFind = await roomData.save();
        responseHandler(socket, 'joinRoom', { success: true, data: {roomFind}, message: 'Room Created!!' });
    } else{
        const userdata = roomFind.users;
        console.log('Users Array:-',userdata);

        let user = false;
        userdata.forEach((element, index) => {
            console.log(element, index);
            if(element == sender){
                user = true;
                console.log('In Room');
            }
        });

        if(user == false){
            roomFind = await Room.findByIdAndUpdate({_id: data.roomID}, {
                $push: {
                    users: sender
                }
            }, { new: true, upsert: true });
            console.log('Added In Room');
        }
    }
    const room = roomFind._id.toString();
    console.log('Room:-', room);

    socket.join(room);
    socket.to(room).emit('res', roomFind);
    responseHandler(socket, 'joinroom', { success: true, message: 'join Room SuccessFully' });
}

module.exports = {
    joinRoom
}