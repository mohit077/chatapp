require('dotenv').config();
require('./config/mongoose').connect();

const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const requestHandler = require('./middleware/requestHandler');
const app = express();
const Defaultdisconnect = require('./controller/default');

const server = http.createServer(app);
const io = socketio(server);

io.on('connection', socket => { 
    console.log(`Connect ${socket.id}`);

    socket.on('req', body => {
        requestHandler(body, socket, io)
    });

    socket.on('disconnect', (reason) => {
        console.log(`disconnect ${socket.id} due to ${reason}`);
        Defaultdisconnect.disconnectHandler(socket);
    })
})

const { API_PORT } = process.env;
const port = process.env.PORT || API_PORT;

server.listen(port, () => {
    console.log(`Server is Running On ${port}`);
});