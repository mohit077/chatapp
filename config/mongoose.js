const mongoose = require('mongoose');

const { MONGO_URL } = process.env;

exports.connect = () => {
    mongoose.connect(MONGO_URL)
        .then(() => console.log('Database Connection Success'))
        .catch(err => console.log('Error Connecting Database', err));
        mongoose.set('debug', true);
}

    